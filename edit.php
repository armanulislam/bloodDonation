<?php
require_once 'vendor/autoload.php';
use App\functions;
$object=new functions();
$object->prepar($_GET);
$object=$object->view();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">Donnar</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="create.php">Create <span class="sr-only">(current)</span></a></li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Actions <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="donatedList.php">Donated List</a></li>
                        <li><a href="#">Another action</a></li>

                    </ul>
                </li>
            </ul>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container">
    <h1 align="center">Edit Donnar profile</h1>
    <form action="update.php" method="post">
        <div class="form-group">
            <label for="name">name:</label>
            <input type="hidden" id="id" name="id" value="<?php echo $object->id?> "/>
            <input type="text" class="form-control" name="name"id="name" value="<?php echo $object->name?> "/>
        </div>
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="hidden" name="id" id="id" value="<?php echo $object->id?>"/>
            <input type="email" name="email" class="form-control" id="email" value="<?php echo $object->email?>"/>
        </div>
        <div class="form-group">
            <label for="mobile">mobile number</label>
            <input type="hidden" id="id" name="id" value="<?php echo $object->id?>"/>
            <input type="number" name="mobile" class="form-control" id="mobile" value="<?php echo $object->mobile?>"/>
        </div>
        <div class="form-group">
            <label for="date">birthday:</label>
            <input type="hidden" name="id" id="id" value="<?php echo $object->id?>"/>
            <input type="date" class="form-control" id="date" name="birthday" value="<?php echo $object->birthday?>"/>
        </div>
        <div class="form-group">
            <label for="location">Location</label>
            <input type="hidden" name="id" id="id" value="<?php echo $object->id?>"/>
            <input type="text" class="form-control" id="location" name="location" value="<?php echo $object->location?>"/>
        </div>
        <div class="form-group">
            <label for="Nid">NID</label>
            <input type="hidden" name="id" id="id" value="<?php echo $object->id?>"/>
            <input type="text" class="form-control" id="nid" name="nid" value="<?php echo $object->nid?>"/>
        </div>
        <div class="form-group">
            <label for="bloodgroup">Select Group:</label>

            <select class="form-control" id="bloodgroup" name="bloodGroup">
                <option>A+</option>
                <option>A-</option>
                <option>B+</option>
                <option>B-</option>
                <option>AB-</option>
                <option>AB+</option>
                <option>O-</option>
                <option>O+</option>
            </select>
        </div>
        <div class="form-group">
            <label for="gender">gender:</label>

            <label for="">
            <input type="radio" name="gender" value="male"> male</label>
            <label for="">
            <input type="radio" name="gender" value="female"> female
            </label>
        </div>
        <button type="submit" class="btn btn-success pull-right">Submit</button>
    </form>
</div>

</body>
</html>