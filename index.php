<?php
require_once 'vendor/autoload.php';
use App\functions;
$object=new functions();
//$object->prepar($_GET);
$object=$object->index();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Blood Donnar list</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand " href="index.php">Donnar</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
<!--                <li class="active"><a href="index.php">Home <span class="sr-only">(current)</span></a></li>-->
                <li><a href="create.php">Create</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Donated  <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="donatedList.php">Donated List</a></li>
                        <li><a href="#">Another action</a></li>


                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="glyphicon glyphicon-download"></i><span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Download As pdf</a></li>
                        <li><a href="#">Download as xml</a></li>
                        <li><a href="#">Download as doc</a></li>

                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container">
    <h1 align="center"> Available Blood Donnar </h1>
    <table class="table table-bordered" >
        <thead>
        <tr>
            <th>Serial</th>
            <th>Name</th>
            <th>BloodGroup</th>
            <th>Email</th>
            <th>Mobile</th>
            <th>Birthday</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $serial=0;
        foreach ($object as $value){
            $serial++;?>
        <tr>
            <td><?php echo $serial?></td>
            <td><?php echo $value->name?></td>
            <td><?php echo $value->bloodGroup?></td>
            <td><?php echo $value->email?></td>
            <td><?php echo $value->mobile?></td>
            <td><?php echo $value->birthday?></td>
            <td>
                <a href="view.php?id=<?php echo $value->id?>" class="btn btn-info" role="button">View</a>
                <a href="edit.php?id=<?php echo $value->id?>" class="btn btn-warning" role="button">Edit</a>
                <a href="delete.php?id=<?php echo $value->id?>" class="btn btn-danger" role="button">Delete</a>
                <a href="donated.php?id=<?php echo $value->id?>" class="btn btn-success" role="button">Donated</a>
            </td>
        </tr>
            <?php }; ?>
        </tbody>
    </table>
</div>
<script>
    $(document).ready(function () {


    })
</script>

</body>
</html>
