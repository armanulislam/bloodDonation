<?php
require_once 'vendor/autoload.php';
use App\functions;
$object=new functions();
$object->prepar($_GET);
$object=$object->view();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Donnar Profile</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">Brand</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li ><a href="index.php">Home <span class="sr-only">(current)</span></a></li>
                <li><a href="create.php">Create</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Actions <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>


                    </ul>
                </li>
            </ul>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container">
    <h1 align="center"> <?php echo $object->name?> </h1>
    <table class="table table-bordered" >
        <thead>
        <tr>


            <th>BloodGroup</th>
            <th>Email</th>
            <th>Mobile</th>
            <th>Birthday</th>
            <th>Location</th>
            <th>NID</th>
            <th>Gender</th>

        </tr>
        </thead>
        <tbody>

            <tr>

                <td><?php echo $object->bloodGroup?></td>
                <td><?php echo $object->email?></td>
                <td><?php echo $object->mobile?></td>
                <td><?php echo $object->birthday?></td>
                <td><?php echo $object->location?></td>
                <td><?php echo $object->nid?></td>
                <td><?php echo $object->gender?></td>
            </tr>

        </tbody>
    </table>
</div>

</body>
</html>
