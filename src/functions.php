<?php
/**
 * Created by PhpStorm.
 * User: Kaiser
 * Date: 2/9/2017
 * Time: 7:54 PM
 */

namespace App;


class functions

{
    public $conn;
    public $name;
    public $email;
    public $gender;
    public $mobile;
    public $picture;
    public $birthday;
    public $location;
    public $nid;
    public $bloodGroup;
    public $alldata=array();
    public $donated_at;
    public $donatedList=array();
    public $id;

    public function __construct()
    {
        $this->conn=mysqli_connect('localhost','root','','todoapps') or die('database connection failed');
    }

    public function prepar($data)
    {
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];
        }
        if(array_key_exists('email',$data)){
            $this->email=$data['email'];
        }
        if(array_key_exists('mobile',$data)){
            $this->mobile=$data['mobile'];
        }
        if(array_key_exists('birthday',$data)){
            $this->birthday=$data['birthday'];

        }
        if(array_key_exists('gender',$data)){
            $this->gender=$data['gender'];
        }
        if(array_key_exists('location',$data)){
            $this->location=$data['location'];
        }
        if(array_key_exists('nid',$data)){
            $this->nid=$data['nid'];
        }
        if(array_key_exists('picture',$data)){
            $this->picture=$data['picture'];
        }
        if(array_key_exists('bloodGroup',$data)){
            $this->bloodGroup=$data['bloodGroup'];
        }
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
    }

    public function store()
    {
        $sql="INSERT INTO `userprofile` (`name`, `email`, `mobile`, `birthday`, `gender`,`location`,`nid`,`bloodGroup`) 
                                    VALUES ('".$this->name."', '".$this->email."', '".$this->mobile."', '".$this->birthday."', 
                                    '".$this->gender."', '".$this->location."', '".$this->nid."', '".$this->bloodGroup."')";
        $reslut=mysqli_query($this->conn,$sql);

    }

    public function index(){

        $sql="SELECT * FROM `userprofile` WHERE `donated_at` is NULL";
        $result=mysqli_query($this->conn,$sql);
        while($row=mysqli_fetch_object($result))
            $this->alldata[]=$row;
        return $this->alldata;
    }

    public function view(){
        $sl="SELECT * FROM `userprofile` WHERE  `id`=".$this->id;
        $rs=mysqli_query($this->conn,$sl);
        $row=mysqli_fetch_object($rs);
        return $row;
    }
    public function update(){
        $sql="UPDATE `userprofile` SET `name` = '".$this->name."', `mobile` = '".$this->mobile."', `birthday` = '".$this->birthday."', 
        `gender` = '".$this->gender."', `location` = '".$this->location."', `nid` = '".$this->nid."', 
        `bloodGroup` = '".$this->bloodGroup."' WHERE `userprofile`.`id` = ".$this->id;
        $reslut=mysqli_query($this->conn,$sql);

    }

    public function donated(){
        $this->donated_at=Time();
        $sql="UPDATE `userprofile` SET `donated_at` = '".$this->donated_at."' WHERE `userprofile`.`id` =".$this->id;
        $result=mysqli_query($this->conn,$sql);
    }

    public function donated_list(){
        $sql="SELECT * FROM `userprofile`WHERE `donated_at`IS NOT NULL";
        $res=mysqli_query($this->conn,$sql);
        while ($row=mysqli_fetch_object($res))
            $this->donatedList[]=$row;
        return $this->donatedList;
    }

    public function delete(){
        $sql="DELETE FROM `userprofile` WHERE `userprofile`.`id` = ".$this->id;
        $result=mysqli_query($this->conn,$sql);

    }

    public function recoverd(){
        $sql="UPDATE `userprofile` SET `donated_at` =NULL WHERE `userprofile`.`id` =".$this->id;
        $result=mysqli_query($this->conn,$sql);
    }


}
